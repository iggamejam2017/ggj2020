extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var HEALTH = 100

# Called when the node enters the scene tree for the first time.
func _ready():
	HEALTH = 100
	pass # Replace with function body.


func ChangeHealth(x):
	print(HEALTH)
	HEALTH += x
	HEALTH = clamp(HEALTH, 0, 100)
	UpdateHealthBar()

func UpdateHealthBar():
	print(HEALTH / 100.0)
	$CanvasLayer/Healthbar.scale.x = HEALTH / 100.0

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
##	global_position = Vector2(get_viewport().get_visible_rect().position.x, 0)
#
#	pass
