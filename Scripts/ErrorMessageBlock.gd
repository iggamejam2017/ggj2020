extends Sprite

class_name   TimedBlockScript   

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var tape = preload("res://Prefabs/playerObjects/TapeFix.tscn")

export(Array, Texture) var textures

export(Array, Texture) var disappearTexture

const x = -40
const y = 20
const xx = 60
const yy = 60

var disappearing = false

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	texture = textures[randi() % textures.size()]
	showError()
	
func showError():
	$AudioStreamPlayer2D.play()

func Disappear():
	
	if disappearing:
		return false
	
	disappearing = true
	
	randomize()
	var time = 0.0
	while time < 0.5:
		time += get_process_delta_time()
		var t = tape.instance()
		self.add_child(t)
		t.position = Vector2(x + randf() * (xx-x), y + randf() * (yy-y))
		yield(get_tree(),"idle_frame")
	
	for i in get_children():
		i.queue_free()
	
	texture = disappearTexture[0]
	yield(get_tree().create_timer(0.5), "timeout")
	get_owner().get_node("StaticBody2D/CollisionShape2D").queue_free()
	texture = disappearTexture[1]
	yield(get_tree().create_timer(0.5), "timeout")
	get_owner().queue_free()
	
	return true
