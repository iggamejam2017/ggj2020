extends Sprite


# Called when the node enters the scene tree for the first time.
#func _ready():
#	pass # Replace with function body.
onready var ArmOffset = $GafferArm.position.x
onready var GunpointOffset = $GafferArm/GunPoint.position.x
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	rotation = -get_parent().rotation
	var dir = (get_owner() as PlayerKinematic).velocity
	if(dir.x < 0):
		flip_h = true
		$GafferArm/Sprite.flip_h = true
		$GafferArm.position.x = -ArmOffset
		$GafferArm/GunPoint.position.x = -GunpointOffset
		$GafferArm/Sprite.position.x = ArmOffset
	else:
		flip_h = false
		$GafferArm/Sprite.flip_h = false
		$GafferArm.position.x = ArmOffset
		$GafferArm/GunPoint.position.x = GunpointOffset
		$GafferArm/Sprite.position.x = -ArmOffset
	pass
