extends Node2D

class_name GAME_MANAGER

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var randomScene1 = preload("res://Levels/RandomLevel1.tscn")
var PaywalLevel = preload("res://Levels/PaywalLevel.tscn")
var dirk1 = preload("res://Levels/Level_dt1.tscn")
var dirk2 = preload("res://Levels/Level_dt2.tscn")
var dirk3 = preload("res://Levels/Level_dt3.tscn")
var manu1 = preload("res://Levels/TutorialLevel.tscn")

var scenes = [manu1, randomScene1, PaywalLevel, dirk2, dirk3, dirk1]

onready var current_scene = $Node2D
var next_scene 
var ground_collider
var ceiling_collider
var left_collider
var gafferMan
var Parallax

var current_end

var constant_nodes: = []

# Called when the node enters the scene tree for the first time.
func _ready():
	ground_collider = current_scene.get_node("FloorCollider")
	ceiling_collider = current_scene.get_node("FloorCollider2")
	left_collider = current_scene.get_node("FloorCollider3")
	gafferMan = current_scene.get_node("GafferMan")
	Parallax = current_scene.get_node("ParallaxBackground")
	
	current_end = current_scene.get_node("END")
	
	constant_nodes.append(ground_collider)
	constant_nodes.append(ceiling_collider)
	constant_nodes.append(left_collider)
	constant_nodes.append(gafferMan)
	constant_nodes.append(Parallax)
	
	for i in constant_nodes:
		setParent(i, self)
	
	CreateNextScene()
	
	pass # Replace with function body.

func setParent(child, new_parent):
	if(child):
		var old_parent = child.get_parent()
		old_parent.remove_child(child)
	new_parent.add_child(child)
	


var sceneid = 0
func CreateNextScene():
	randomize()
	var i = randi() % scenes.size()
#	i = 2
	next_scene = scenes[sceneid % scenes.size()].instance()
	sceneid += 1

	
	next_scene.position.x = current_end.global_position.x
	
	for i in next_scene.get_children():
		if i.is_in_group("testing"):
			i.queue_free()
	
	current_end = next_scene.get_node("END")
	
	self.add_child(next_scene)
	
	print("Creating scene %s at %s" % [i, current_end.position.x])
	

func RemoveCurrentScene():
	current_scene.queue_free()
	current_scene = next_scene
	left_collider.position.x = current_scene.position.x - 50
	ceiling_collider.position.x = current_scene.position.x
	ground_collider.position.x = current_scene.position.x

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	
	var distanceToEnd = current_end.global_position.x - gafferMan.get_node("Player").global_position.x
	if distanceToEnd <= 2000:
		print(distanceToEnd)
		RemoveCurrentScene()
		CreateNextScene()
	
	pass
