extends Node

# Main Screen
func _on_CreditsButton_pressed():
	get_tree().change_scene("res://Scenes/Credits_Screen.tscn")

# Credits Screen
func _on_Backbutton_pressed():
	get_tree().change_scene("res://Scenes/Main_Screen.tscn")


func _on_Start_pressed():
	get_tree().change_scene("res://Levels/GameScene_1.tscn")
