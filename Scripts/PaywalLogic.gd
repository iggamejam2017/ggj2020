extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var hasSpawned = false

var player

# Called when the node enters the scene tree for the first time.
func _ready():
	
	print(player)
	for c in $StartCoins/Coins.get_children():
		c.hide()
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if hasSpawned:
		var missing = $StartCoins/Coins.get_child_count()
		print("Missing %s coins" % missing)
		if missing == 0:
			get_tree().call_group("player", "coins_collected")
			queue_free()
	pass

func SpawnCoins():
	for c in $StartCoins/Coins.get_children():
		c.show()
	pass

func _on_StartCoins_body_entered(body):
	if body.get_groups().has("player") and !hasSpawned:
		hasSpawned = true
		get_tree().call_group("player", "coins_spawned")
		SpawnCoins()
	pass # Replace with function body.
