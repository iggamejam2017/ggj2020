extends Node2D

export var maxItems = 12
var speed = 10
var nodes = []
var currentStart = 0

func _ready():
	var strich = load("res://Prefabs/Strich.tscn")
	
	#init
	for i in range(31):
		var node = strich.instance()
		var scaleX = -375
		var placement_pos = Vector2(scaleX + 25*i, 0)
		add_child(node)
		node.hide()
		node.get_node("Collider").set_disabled(true)
		node.translate(placement_pos)
		nodes.push_back(node)
	$Timer.set_wait_time(1/speed)
	$Timer.start()


func _on_Timer_timeout():
	
	for i in nodes.size():
			nodes[i].hide()
			nodes[i].get_node("Collider").set_disabled(true)
	
	for i in range(maxItems):
		var idxs = (currentStart + maxItems + i) % nodes.size()
		nodes[idxs].show()
		nodes[idxs].get_node("Collider").set_disabled(false)
	
	currentStart = (currentStart + 1) % nodes.size()
#	print(currentStart)
