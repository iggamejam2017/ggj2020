extends KinematicBody2D

export var speed = -300 
export var height_boundary = 200
export var width_boundary = 500
export var despawnAfter = 1000

var startPos
var moveDown = true

func _ready():
	startPos = position
		
func _flip_directionX():
	speed = speed * -1

func _physics_process(delta):
	if(position.x > (startPos.x + width_boundary) || position.x < (startPos.x - width_boundary)):
		_flip_directionX()
	
	if (moveDown && (position.y > (startPos.y + height_boundary))):
		moveDown = false
		height_boundary = height_boundary * -1
	elif (!moveDown && (position.y < startPos.y)):
		moveDown = true
		
	var move = Vector2(speed * delta, height_boundary * delta)
	move_and_collide(move)
	
	if(position.x > (startPos.x + despawnAfter) || position.x < (startPos.x - despawnAfter)):
		queue_free()


func _on_Area2D_body_entered(body):
	_flip_directionX()
