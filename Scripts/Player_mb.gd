extends RigidBody2D


export var force = 300
export var maxSpeed = 50
# Called when the node enters the scene tree for the first time.

func _input(event):
	if event.is_action_pressed("ui_right"):
		set_applied_force(Vector2(force,0))
	if event.is_action_pressed("ui_left"):
		set_applied_force(Vector2(-force,0))
	if event.is_action_pressed("ui_up"):
		set_applied_force(Vector2(0,force))
	if event.is_action_pressed("ui_down"):
		set_applied_force(Vector2(0,-force))
	
