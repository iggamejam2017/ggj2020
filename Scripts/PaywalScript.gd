extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var Brick = preload("res://Prefabs/helpers/PaywalBrick.tscn")

const height = 100
const count = 20

var bricks = []

# Called when the node enters the scene tree for the first time.
func _ready():
	
	for i in range(count):
		var b = Brick.instance()
		b.position = Vector2(0, ((-height * count / 2.0)) + i * height)
		print(b.position)
		bricks.append(b)
		add_child(b)
		
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
