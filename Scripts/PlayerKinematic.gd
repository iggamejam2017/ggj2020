extends KinematicBody2D

class_name PlayerKinematic

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var Fadenkreuz = get_node("../GafferTapeGun/Fadenkreuz")
onready var gunPoint = $CollisionShape2D/FlyingSprite/GafferArm/GunPoint

onready var tape = preload("res://Prefabs/playerObjects/TapeFix.tscn")
onready var Healthbar = $Health

var frozen = false
var velocity = Vector2(0,0)
var physics = true
export var gravity = Vector2(0, 9.81) * 50
export(float, 0.1, 0.999) var ground_drag = 0.95

# Called when the node enters the scene tree for the first time.
func _ready():
	$DeathAnimation.playing = false
	$DeathAnimation.hide()
	pass # Replace with function body.


func rotateArm(target):
	$CollisionShape2D/FlyingSprite.show()
	$CollisionShape2D/AnimatedSprite.hide()
	
	$CollisionShape2D/FlyingSprite/GafferArm.rotation = (position - target).angle() if $CollisionShape2D/FlyingSprite.flip_h else (target - position).angle() 
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	
	if physics and !frozen:
		velocity += delta * gravity
		
		if is_on_floor():
			velocity *= Vector2(ground_drag, 1)
	#	var motion = velocity * delta
		
		velocity = move_and_slide(velocity, Vector2(0, -1))
		
		$CollisionShape2D/FlyingSprite.hide()
		$CollisionShape2D/AnimatedSprite.show()
	
#	print(velocity)
	
	pass

func KillPlayer():
	frozen = true
	$CollisionShape2D/FlyingSprite.hide()
	$CollisionShape2D/AnimatedSprite.hide()
	$DeathAnimation.frame = 0
	$DeathAnimation.play()
	$DeathAnimation.show()
	yield(get_tree().create_timer(2.5), "timeout")
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://Scenes/Main.tscn")

func _on_Area2D_body_entered(body):
#	print("BODY ENTERED")
	print(body.get_name())
	if body.get_groups().has("kill"):
		print("Take Damage")
		Healthbar.ChangeHealth(-20)
		$DamageSound.play()
		if Healthbar.HEALTH <= 0.1:
			KillPlayer()
		
	else:
		frozen = true
		yield(get_tree(),"idle_frame")
		frozen = false
		pass
	pass # Replace with function body.


func _on_DeathAnimation_animation_finished():
	$DeathAnimation.frame = 5
	$DeathAnimation.playing = false
	randomize()
	var time = 0.0
	while time < 0.5:
		time += get_process_delta_time()
		var t = tape.instance()
		self.add_child(t)
		t.position = Vector2((randf() - 0.5) * 80 , (randf() - 0.5) * 80)
		yield(get_tree(),"idle_frame")
	pass # Replace with function body.

func coins_collected():
	$PaywallDone.play()
	$PaywalHUD/coin_collector.hide()
	
func coins_spawned():
	$PaywalHUD/coin_collector.show()
	

