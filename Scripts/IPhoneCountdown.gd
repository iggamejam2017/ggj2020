extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


export(Array, Texture) var textures
var x = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	Countdown()
	pass # Replace with function body.

func Countdown():
	while(x <= textures.size()):
		yield(get_tree().create_timer(1), "timeout")
		x += 1
		
		texture = textures[x]
	
	get_parent().queue_free()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
