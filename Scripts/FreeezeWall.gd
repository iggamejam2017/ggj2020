extends Area2D

var collidedPlayer

func _on_FreezeWall_body_entered(body):
	if(body.is_in_group("player")):
		collidedPlayer = body as PlayerKinematic
		
		collidedPlayer.frozen = true
		
#		(collidedPlayer.Fadenkreuz as Fadenkreuz).removeTape()
#		body.set_sleeping(true)
		yield(get_tree().create_timer(0.5), "timeout")
		
		collidedPlayer.frozen = false
		collidedPlayer.velocity = Vector2()
		queue_free();
