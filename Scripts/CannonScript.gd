extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


var projectile = preload("res://Prefabs/helpers/CannonProjectile.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	fireCoroutine()
	pass # Replace with function body.


func fireCoroutine():
	randomize()
	yield(get_tree().create_timer(randi() % 6), "timeout")
	
	while true:
		var p = projectile.instance()
		self.add_child(p)
		p.position = $ProjectileSpawn.position
		(p as CannonProjectile).fire(Vector2(-1, 0))
		yield(get_tree().create_timer(4), "timeout")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
