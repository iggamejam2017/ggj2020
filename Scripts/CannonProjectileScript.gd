extends Node2D

class_name CannonProjectile

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var direction: = Vector2(0,0)
const maxDist = 1000
var dist = 0;
export var speed: = 120

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func fire(dir):
	
	yield(get_tree().create_timer(0.5), "timeout")
	$AudioStreamPlayer2D.play()
	direction = dir
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	position += direction * delta * speed
	rotation += delta * 2
	
	dist += (direction * delta * speed).length()
	if dist > maxDist:
		queue_free()
	
	pass
