extends Area2D

class_name Fadenkreuz

var lastClick = Vector2(0,0)
var updateAim = false
var line
var hit_pos = Vector2(0,0)

var previous = Vector2(0,0)

onready var RandomTapeScene = preload("res://Prefabs/playerObjects/TapeFix.tscn")
onready var TapeContainer = get_node("../../TapeContainer")

onready var errorScene = preload("res://Prefabs/walls/TimedBlock.tscn")

onready var aimVisualizer = get_node("../AimVisualizer")
onready var container = get_node("../../GafferTapeGun")
onready var player = get_tree().get_nodes_in_group("player")[0] as PlayerKinematic


# Called when the node enters the scene tree for the first time.
func _ready():
	line = Line2D.new()
	line.add_point(Vector2(0,0))
	line.add_point(Vector2(0,100))
	line.width = 10

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _input(_event):
	if Input.is_action_just_pressed("click"):
#		print("Mouse at: ", get_global_mouse_position())
		lastClick = get_global_mouse_position()
		updateAim = true
	if Input.is_action_just_released("click"):
		removeTape()

func removeTape():
	if hit_pos:
		hit_pos = null
		container.hide()
		var p = player as PlayerKinematic
#		p.velocity = p.position - previous

		p.physics = true
	
	
	pass
	
func calculateTapeDragCoroutine():
	
	var standard_length = (player.position - hit_pos).length()
	var p = player as PlayerKinematic
	p.physics = false
	
	var velocity = p.velocity
	var _acc = velocity;
	
	previous = player.position
	standard_length /= 1
	
	while hit_pos:
		if p.frozen:
			removeTape()
			return
#		print("Taped")
		container.show()
		visualizeTapeState()
		
		var delta = get_physics_process_delta_time()
		
		# Constraints
		var dir = hit_pos - player.position;
		var factor = (dir.length() - standard_length) / (2 * dir.length())
		factor = min(factor, 0.3)
		
		var correction = dir * factor * 0.9
		
		var pos = player.position + correction
#		player.position += correction
		## HitPos should not move
		
		# Integrator
		var nextPos = pos + (pos - previous) * 0.99 + (Vector2(0, 50) * delta)
		
		p.velocity = (nextPos - pos) * 50
#		p.velocity = Vector2(0,0)
		
		previous = pos
		p.move_and_collide(nextPos - p.position)
#		player.position = nextPos
		
		standard_length = max(10, standard_length * 0.98)
		
		p.rotateArm(hit_pos)
		
#		rb.position += d * velocity;
#		zz(standard_length)
		
		yield(get_tree(), "physics_frame")
	
	pass
	
func visualizeTapeState():
	aimVisualizer.set_point_position(0, player.gunPoint.global_position)
	aimVisualizer.set_point_position(1, position)

func DeleteWall (col):
	print(col)
	col.get_owner().get_node("popup").show()
	col.get_owner().get_node("box").hide()
	col.get_owner().get_node("box").get_node("CollisionShape2D").set_disabled(true)
#	col.set_disabled(true)
	

func CreateTapeFix(pos, col):
	var tape = RandomTapeScene.instance()
#	tape.position = pos
#	tape.position = Vector2()
	var tape_container = null
	if col.has_node("TapeContainer"):
		tape_container = col.get_node("TapeContainer")
	else:
		tape_container = Node2D.new()
		col.add_child(tape_container)
		tape_container.set_global_scale(Vector2(1, 1))
		tape_container.position = Vector2()
	tape_container.add_child(tape)
	tape.set_global_position(pos)

func _physics_process(_delta):
	update()
	if updateAim:
		updateAim = false
		var space_state = get_world_2d().direct_space_state
		# use global coordinates, not local to node
		var result = space_state.intersect_ray(player.gunPoint.global_position, player.gunPoint.global_position + (lastClick - player.gunPoint.global_position).normalized() * 1500, [player] )
		if result:
			hit_pos = result.position
			position = result.position
			
			# StaticBody2D
			if (result.collider.is_in_group("nowall")):
				DeleteWall (result.collider)
			elif (result.collider.is_in_group("air")):
				pass
			elif (result.collider.is_in_group("timedblock")):
				var tt = result.collider.get_owner().get_node("StaticBody2D/Sprite") as TimedBlockScript
				if tt.Disappear():
					RespawnError(result.collider.get_owner().get_parent(), result.collider.global_position)
				calculateTapeDragCoroutine()
			else:
				CreateTapeFix(result.position, result.collider)
				calculateTapeDragCoroutine()
		else:
			hit_pos = null

func RespawnError(parent, pos):
	
	yield(get_tree().create_timer(3), "timeout")
	
	var e = errorScene.instance()
	parent.add_child(e)
	e.global_position = pos

func _draw():
#	draw_line(Vector2(), get_tree().get_nodes_in_group("player")[0].position, Color.red)
#	draw_circle((hit_pos - lastClick).rotated(-rotation), 5, Color.red)
	pass
