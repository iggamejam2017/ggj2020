extends AnimatedSprite


# Called when the node enters the scene tree for the first time.
#func _ready():
#	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	rotation = -get_parent().rotation
	var dir = (get_owner() as PlayerKinematic).velocity
	if(dir.x >50|| dir.x < -50):
		if(dir.x < 0):
			flip_h = true
		else:
			flip_h = false
