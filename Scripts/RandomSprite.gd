extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export(Array, Texture) var textures

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	texture = textures[randi() % textures.size()]
	flip_h = randf() > 0.5
	flip_v = randf() > 0.5
	rotation = randf() * 3.141592653589793 * 2
	scale = Vector2(1, 1) * (0.3 + randf() * 0.2)
		
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
