extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


export(Array, Texture) var textures

var intensity = 1
var time = 0
var speed = 0
const width = 270

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	texture = textures[randi() % textures.size()]
	intensity = randf() * width / 3.0
	time = randf() * 2
	speed = 1 + randf() * 0.5
		
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	time += delta
	
	get_parent().position.x = intensity * sin(time * speed)
	
	pass
