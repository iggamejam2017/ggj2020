extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

const minHeight = 20
const maxHeight = 130

const minDistance = 600
const deltaDistance = 1500

var Lamp = preload("res://Prefabs/Lamp.tscn")

var lamps = []
var lastLamp

onready var player = get_owner().get_node("GafferMan/Player")

# Called when the node enters the scene tree for the first time.
func _ready():
	SpawnLamp(0)
	pass # Replace with function body.

func SpawnLamp(lastX):
	randomize()
	var l = Lamp.instance()
	l.global_position = Vector2(lastX + minDistance + randf() * deltaDistance, minHeight + randf() * (maxHeight - minHeight))
	add_child(l)
	lamps.append(l)
	lastLamp = l
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if player.position.x - lastLamp.position.x <= 2000:
		SpawnLamp(lastLamp.position.x)
	
	pass
